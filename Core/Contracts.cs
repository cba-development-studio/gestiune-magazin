﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Core
{
    public static class Contracts
    {
        [DebuggerStepThrough]
        public static void Require(bool precondition, string message = "")
        {
            if (!precondition)
                throw new ContractException(message);
        }

        [DebuggerStepThrough]
        public static void NotNull(object obj)
        {
            if (obj == null)
                throw new ContractException("Argument Null Exception!");
        }

        [DebuggerStepThrough]
        public static void NotNullOrEmpty(string obj)
        {
            if (obj == null || obj.Trim() == "")
                throw new ContractException("Argument Null Exception!");
        }
    }

    [Serializable]
    public class ContractException : Exception
    {
        public ContractException()
        {
        }

        public ContractException(string message)
            : base(message)
        {
        }

        public ContractException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected ContractException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
