﻿using Core;

namespace Domain.AggregatesModel.InventoryAggregate
{
    public interface IInventoryRepository : IRepository<Inventory>
    {
        Inventory GetInventory(long productId);
        void Update(Inventory inventory);
    }
}
