﻿using System.Collections.Generic;

using Core;

namespace Domain.AggregatesModel.InventoryAggregate
{
    public class Inventory : IAggregateRoot
    {
        public Inventory(long productId)
        {
            this._transactions = new List<Transaction>();
            this._productId = productId;
            this.Quantity = 0;
        }

        public Inventory(long productId, int quantity) : this(productId)
        {
            this.Quantity = quantity;
        }

        private readonly long _productId;
        public int Quantity { get; private set; }
        private readonly List<Transaction> _transactions;
        public IReadOnlyCollection<Transaction> Transactions => this._transactions?.AsReadOnly();

        public void Add(int quantity)
        {
            Contracts.Require(quantity > 0);

            Transaction transaction = new Transaction(this._productId, TransactionType.Intrare, quantity);
            this._transactions.Add(transaction);
            this.Quantity += quantity;
        }
        public void Extract(int quantity)
        {
            Contracts.Require(quantity > 0);
            Contracts.Require(this.Quantity >= quantity);

            Transaction transaction = new Transaction(this._productId, TransactionType.Iesire, quantity);
            this._transactions.Add(transaction);
            this.Quantity -= quantity;
        }
    }
}
