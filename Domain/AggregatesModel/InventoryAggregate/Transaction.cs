﻿using System;

using Core;

namespace Domain.AggregatesModel.InventoryAggregate
{
    public class Transaction : Entity
    {
        public Transaction(long productId, TransactionType type, int quantity)
        {
            this.ProductId = productId;
            this.Type = type;
            this.Quantity = quantity;
            this.Date = DateTime.UtcNow;
        }

        protected Transaction()
        {
        }

        public long ProductId { get; private set; }

        public DateTime Date { get; private set; }
        public int Quantity { get; private set; }
        public TransactionType Type { get; private set; }
    }
}
