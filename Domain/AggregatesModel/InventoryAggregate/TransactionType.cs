﻿using System;
using System.Linq;

using Core;

namespace Domain.AggregatesModel.InventoryAggregate
{
    public class TransactionType : Enumeration
    {
        public static TransactionType Intrare = new TransactionType(1, "Intrare");
        public static TransactionType Iesire = new TransactionType(2, "Iesire");
        public TransactionType(int id, string name) : base(id, name)
        {
        }

        public static TransactionType Get(int id)
        {
            TransactionType type = GetAll<TransactionType>().FirstOrDefault(x => x.Id == id);
            if (type == null)
                throw new Exception($"Item is missing for id: {id}");

            return type;
        }
    }
}
