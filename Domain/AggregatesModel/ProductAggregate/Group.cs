﻿using System;
using System.Linq;

using Core;

namespace Domain.AggregatesModel.ProductAggregate
{
    public class Group : Enumeration
    {
        public static Group bluzaDama = new Group(1, "Bluza Dama");
        public static Group bluzaBaieti = new Group(2, "Bluza Baieti");
        public static Group blugiDama = new Group(3, "Blugi Dama");
        public static Group blugiBaieti = new Group(4, "Blugi Baieti");

        public Group(int id, string name) : base(id, name)
        {
        }

        public static Group Get(int id)
        {
            Group group = GetAll<Group>().FirstOrDefault(x => x.Id == id);
            if (group == null)
                throw new Exception($"Item is missing for id: {id}");

            return group;
        }
    }
}
