﻿using Core;

namespace Domain.AggregatesModel.ProductAggregate
{
    public interface IProductRepository : IRepository<Product>
    {
        void Update(Product product);
    }
}
