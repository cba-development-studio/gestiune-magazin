﻿
using Core;

namespace Domain.AggregatesModel.ProductAggregate
{
    public class Product : Entity, IAggregateRoot
    {
        public Product(string name, decimal price, Group group)
        {
            Contracts.NotNullOrEmpty(name);
            Contracts.Require(price > 0, "Price must be grader than 0");
            Contracts.NotNull(group);

            this.Name = name;
            this.Price = price;
            this._groupId = group.Id;
        }
        public Product(long id, string name, decimal price, Group group) : this(name, price, group)
        {
            Contracts.Require(id > 0, "Id must be grader than 0");
            this.Id = id;
        }

        public string Name { get; private set; }
        public decimal Price { get; private set; }

        private int _groupId;
        public Group Group => Group.Get(this._groupId);
        public void ChangeGroup(Group group)
        {
            Contracts.NotNull(group);

            this._groupId = group.Id;
        }
    }
}
