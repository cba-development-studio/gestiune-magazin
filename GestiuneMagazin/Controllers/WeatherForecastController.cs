﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Domain.AggregatesModel.InventoryAggregate;
using Domain.AggregatesModel.ProductAggregate;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GestiuneMagazin.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IProductRepository productRepository;
        private readonly IInventoryRepository inventoryRepository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,
            IProductRepository productRepository,
            IInventoryRepository inventoryRepository)
        {
            _logger = logger;
            this.productRepository = productRepository;
            this.inventoryRepository = inventoryRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            var rng = new Random();


            var inventory = this.inventoryRepository.GetInventory(12);
            inventory.Add(15);
            this.inventoryRepository.Update(inventory);
            await this.inventoryRepository.UnitOfWork.SaveChangesAsync();

            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
