﻿namespace Infrastructure.Entities
{
    public class GroupEntity
    {
        protected GroupEntity() { }

        public GroupEntity(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
    }
}
