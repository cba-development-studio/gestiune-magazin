﻿namespace Infrastructure.Entities
{
    public class ProductEntity
    {
        protected ProductEntity() { }
        public ProductEntity(string name, decimal price, int groupId)
        {
            Name = name;
            Price = price;
            GroupId = groupId;
        }

        public long Id { get; private set; }

        public string Name { get; private set; }
        public decimal Price { get; private set; }
        public int GroupId { get; set; }

        public void UpdateValues(string name, decimal price, int groupId)
        {
            Name = name;
            Price = price;
            GroupId = groupId;
        }
    }
}
