﻿using System;

namespace Infrastructure.Entities
{
    public class TransactionEntity
    {
        protected TransactionEntity() { }
        public TransactionEntity(long productId, DateTime date, int quantity, int type)
        {
            ProductId = productId;
            Date = date;
            Quantity = quantity;
            Type = type;
        }

        public long Id { get; private set; }

        public long ProductId { get; private set; }
        public DateTime Date { get; private set; }
        public int Quantity { get; private set; }
        public int Type { get; private set; }
    }
}
