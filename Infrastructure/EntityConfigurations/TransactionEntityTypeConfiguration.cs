﻿using Infrastructure.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations
{
    internal class TransactionEntityTypeConfiguration : IEntityTypeConfiguration<TransactionEntity>
    {
        public void Configure(EntityTypeBuilder<TransactionEntity> builder)
        {
            builder.ToTable("Transactions");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.ProductId);
            builder.Property(x => x.Date);
            builder.Property(x => x.Quantity);
            builder.Property(x => x.Type);

            builder.HasOne<ProductEntity>().WithMany();
            builder.HasOne<TransactionTypeEntity>().WithMany();
        }
    }
}
