﻿using System.Linq;

using Core;

using Domain.AggregatesModel.InventoryAggregate;

using Infrastructure.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations
{
    internal class TransactionTypeEntityTypeConfiguration : IEntityTypeConfiguration<TransactionTypeEntity>
    {
        public void Configure(EntityTypeBuilder<TransactionTypeEntity> builder)
        {
            builder.ToTable("TransactionTypes");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedNever();

            builder.Property(x => x.Name);

            builder.HasData(Enumeration.GetAll<TransactionType>().Select(x => new TransactionTypeEntity(x.Id, x.Name)));
        }
    }
}
