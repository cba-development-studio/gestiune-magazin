﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using Core;

using Infrastructure.Entities;
using Infrastructure.EntityConfigurations;

using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class GestiuneContext : DbContext, IUnitOfWork
    {
        public DbSet<TransactionEntity> Transactions { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<GroupEntity> Groups { get; set; }
        public DbSet<TransactionTypeEntity> TransactionTypes { get; set; }

        public GestiuneContext(DbContextOptions<GestiuneContext> options) : base(options)
        {
            Debug.WriteLine("GestiuneContext::ctor ->" + GetHashCode());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TransactionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new GroupEntityTypeConfiguration());
        }

        public Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
