﻿using System.Linq;

using Core;

using Domain.AggregatesModel.InventoryAggregate;

using Infrastructure.Entities;

namespace Infrastructure.Repositories
{
    public class InventoryRepository : IInventoryRepository
    {
        private readonly GestiuneContext _context;

        public InventoryRepository(GestiuneContext context)
        {
            this._context = context;
        }

        public IUnitOfWork UnitOfWork => this._context;

        public Inventory GetInventory(long productId)
        {
            var quantity = this._context.Transactions
                 .Where(x => x.ProductId == productId)
                 .Sum(x => x.Type == TransactionType.Iesire.Id ? -x.Quantity : x.Quantity);

            return new Inventory(productId, quantity);
        }

        public void Update(Inventory inventory)
        {
            var transactions = inventory.Transactions.Select(x => new TransactionEntity(x.ProductId, x.Date, x.Quantity, x.Type.Id));
            this._context.Transactions.AddRange(transactions);
        }
    }
}
