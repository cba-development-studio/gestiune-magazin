﻿using System.Linq;

using Core;

using Domain.AggregatesModel.ProductAggregate;

using Infrastructure.Entities;

namespace Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly GestiuneContext _context;

        public ProductRepository(GestiuneContext context)
        {
            this._context = context;
        }

        public IUnitOfWork UnitOfWork => this._context;

        private void Insert(Product product)
        {
            ProductEntity entity = new ProductEntity(product.Name, product.Price, product.Group.Id);
            this._context.Products.Add(entity);
        }

        public void Update(Product product)
        {
            ProductEntity entity = this._context.Products.FirstOrDefault(x => x.Id == product.Id);
            if (entity == null)
                this.Insert(product);
            else
                entity.UpdateValues(product.Name, product.Price, product.Group.Id);
        }
    }
}
