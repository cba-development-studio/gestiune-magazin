﻿
using Core;

using Domain.AggregatesModel.InventoryAggregate;

using NUnit.Framework;

namespace UnitTest
{
    public class InventoryAggregateTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Create_Inventory_success()
        {
            //Arrange
            long productId = 1;

            //Act 
            Inventory fakeInventory = new Inventory(productId);

            //Assert
            Assert.NotNull(fakeInventory);
        }

        [Test]
        public void Create_Inventory_Without_transactions()
        {
            //Arrange
            long productId = 1;
            int expectedQuantity = 0;

            //Act 
            Inventory fakeInventory = new Inventory(productId);

            //Assert
            Assert.NotNull(fakeInventory);
            Assert.AreEqual(expectedQuantity, fakeInventory.Quantity);
        }

        [Test]
        [TestCase(1, 2)]
        [TestCase(2, 5)]
        [TestCase(3, 21)]
        public void Create_Inventory_With_DefaultValue_Without_transactions(long productId, int quatity)
        {
            //Arrange
            int expectedQuantity = quatity;

            //Act 
            Inventory fakeInventory = new Inventory(productId, quatity);

            //Assert
            Assert.NotNull(fakeInventory);
            Assert.AreEqual(expectedQuantity, fakeInventory.Quantity);
        }

        [Test]
        [TestCase(1, 2, 2)]
        [TestCase(2, 5, 3)]
        [TestCase(3, 21, 1)]
        public void Create_Inventory_With_DefaultValue_With_transactions(long productId, int defaultQuatity, int newQuantity)
        {
            //Arrange
            int expectedQuantity = defaultQuatity + newQuantity;

            //Act 
            Inventory fakeInventory = new Inventory(productId, defaultQuatity);
            fakeInventory.Add(newQuantity);

            //Assert
            Assert.NotNull(fakeInventory);
            Assert.AreEqual(expectedQuantity, fakeInventory.Quantity);
        }

        [Test]
        [TestCase(1, 2, 2)]
        [TestCase(2, 5, 3)]
        [TestCase(3, 21, 1)]
        public void Create_Inventory_Extract_Success(long productId, int defaultQuatity, int extractQuantity)
        {
            //Arrange
            int expectedQuantity = defaultQuatity - extractQuantity;

            //Act 
            Inventory fakeInventory = new Inventory(productId, defaultQuatity);
            fakeInventory.Extract(extractQuantity);

            //Assert
            Assert.NotNull(fakeInventory);
            Assert.AreEqual(expectedQuantity, fakeInventory.Quantity);
        }

        [Test]
        [TestCase(1, 2, 3)]
        [TestCase(2, 5, 11)]
        [TestCase(3, 21, 30)]
        public void Create_Inventory_Extract_ThrowExceptio(long productId, int defaultQuatity, int extractQuantity)
        {
            //Arrange
            int expectedQuantity = defaultQuatity;

            //Act 
            Inventory fakeInventory = new Inventory(productId, defaultQuatity);
            Assert.Throws<ContractException>(() => fakeInventory.Extract(extractQuantity));

            //Assert
            Assert.NotNull(fakeInventory);
            Assert.AreEqual(expectedQuantity, fakeInventory.Quantity);
        }
    }
}
