using Core;

using Domain.AggregatesModel.ProductAggregate;

using NUnit.Framework;

namespace UnitTest
{
    public class ProductAggregateTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Create_Product_success()
        {
            //Arrange    
            string productName = "FakeProductName";
            int unitPrice = 12;

            //Act 
            Product fakeProduct = new Product(1, productName, unitPrice, Group.bluzaDama);

            //Assert
            Assert.NotNull(fakeProduct);
        }

        [Test]
        public void Invalid_product_name()
        {
            //Arrange    
            string productName = "";
            int unitPrice = 12;

            //Act - Assert
            Assert.Throws<ContractException>(() => new Product(1, productName, unitPrice, Group.bluzaDama));
        }

        [Test]
        public void Invalid_product_price()
        {
            //Arrange    
            string productName = "FakeProductName";
            int unitPrice = 0;

            //Act - Assert
            Assert.Throws<ContractException>(() => new Product(1, productName, unitPrice, Group.bluzaDama));
        }

        [Test]
        public void Invalid_group()
        {
            //Arrange    
            string productName = "FakeProductName";
            int unitPrice = 12;

            //Act - Assert
            Assert.Throws<ContractException>(() => new Product(1, productName, unitPrice, null));
        }
    }
}